import { Component, OnInit } from '@angular/core';
import {ChatService} from '../chat.service';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html'
})
export class ChatComponent implements OnInit {

  constructor(private chatService:ChatService) { }
  messages=[{message:"hey",from:2}];

  getMessage(event,text):void{
    if(event.key=="Enter"){
      this.messages.push({message:text,from:1});
      this.messages.push({message:this.chatService.getMessage(text),from:2});
    }
    // if(event.key=="Enter"){
    //   this.chatService.getMessage(text).then(message => {console.log(message);});
    // }
  }

  // addMessage(event,message:string){
  //     if(event.key=="Enter"){
  //       this.messages.push({message:message,from:1});
  //       console.log(this.messages);
  //     }
  // }

  ngOnInit() {
    // this.getMessages();
  }

}
