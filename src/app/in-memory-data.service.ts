  import { InMemoryDbService } from  'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const messages =[
      { message: "Hey human",  from: 2 },
      { message: "krass",  from: 1 }
    ];
    return {messages};
  }
}
