import { FrontendDynamicaiPage } from './app.po';

describe('frontend-dynamicai App', () => {
  let page: FrontendDynamicaiPage;

  beforeEach(() => {
    page = new FrontendDynamicaiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
